# PackageSorter

A CLI program that grabs the package list for a given repo and outputs the ten applications with the most packages.

*Total time taken:* Approximately 25 hours.

## How to use

### Installing
Run the following in the project root folder:

```
python setup.py install
```

### Running the tests
Run the following in the project root folder:

```
python tests.py
```

### Using the application
Enter the following to get help (this will include a list of the available processors and pseudo-processors):

```
packsort -h
```

Enter the following to run the application:

```
packsort <proc>
```

\<proc> is the name of the processor or pseudo-processor from:

all udeb-amd64 amd64 amd64 arm64 armel armf i386 mips64el mipsel s390x amd64 arm64 armel armf i386 mips64el mipsel s390x udeb-amd64 udeb-arm64 udeb-armel udeb-armf udeb-i386 udeb-mips64el udeb-mipsel udeb-ppc64el udeb-s390x

To change the repo and/or the list of processors, amend the *packsort.cfg* file

***
## Steps taken when creating project

### Research

The first thing I always do is to ensure I have all of the necessary (initial)
knowledge for creating the project.  I spent a couple of hours researching/
revising how to:

1. Create command-line tools using *argparse*,
2. Get the regex working for grabbing the relevant info from the repo files,
3. Looked into progress bars, which I eventually didn't use as I wanted to restrict myself to the standard library as much as possible.
4. Looked into the fastest sort algorithms for this task.

### Design

Before starting to code, I did some UML design work:

        # Package Sorter

        ## USE CASE Description

        1. The program takes in a processor type.  The first line in a file is loaded with the URL.  A filename is created and used to download the file.  
        The file is unzipped.  The app names and counts are extracted from the unzipped file.  The names and counts are sorted and the top ten are 
        extracted.  The top ten is displayed.  The program ends.

        ## Triggers

        1. The user gives the name of a processor

        ## Actors

        1. CLI program
        2. Online file
        3. Count list
        4. Config file with URL and list of processors

        ## Preconditions

        1. A processor name is passed in

        ## Goals

        1. Pass back a list of the top ten package counts for the given
        processor.

        ## Steps of Execution

        1. Check the Command is in the correct format.  If it is not, display a help
        message and exit the program.
        2. Check that the processor entered by the user is in the list.  If it is
        not in the list then output a list of valid processors and exit the program.
        3. Download the file with relevant error checking.  Allow the user to retry
        the download (or exit) if it did not work.
        4. Unzip the file
        5. Extract the repo app names and count their packages (package count list)
        6. Sort the package count list.
        7. Extract the top ten from the package count list
        8. print the top ten list
        9. exit the program

### UML Diagrams

#### Object Model

![Object Model](docs/UML/ObjectModel.png)

#### Class Diagram

![Class Diagram](docs/UML/ClassDiagram.png)

#### Sequence Diagram

![Sequence Diagram](docs/UML/sequence_diagram.png)

### Implementation

For the implementation steps, I did the following:
1. I got a basic script working, that downloaded the files and unzipped them into the correct folder, just to get a feel for what needs to be done.
2. I created a gitlab repository and added a virtualenv and set up a basic folder structure (using https://github.com/navdeep-G/samplemod.git). 
3. I then built the main class structure from the design, including docstrings, *\_\_repr__()* methods and comments. 
4. I added some asserts to all of the methods that could use them.  Later I would normally transfer all of these to the unit test function.  For this application, I
only did unit tests for the Repo class, just to show the functioning.  
5. I then filled in all of the methods and got the program functioning.  I did the main application, followed by the configuration class, the command and finally brought it all together with the repo class.
6. Next, I added testing using the unittest library.  I only did limited testing here (to save time), but would have done much more for a real-world application.
7. Next, I got the setup.py working.
8. Finally, I updated all of the UMLs, docs and notes.

### Future updates

1. Complete the unit testing (only Repo was tested here),
2. Implement some integration testing as well,
2. Download the list of processors/pseudo-processors automatically from the repo instead of listing them in the configuration file,
3. Inherit GzipManager from FileManager.