
"""This module contains the following
Classes
-------
Command
"""

import argparse
import sys

class Command():
    """Class to process the command line for the packsort app."""

    def __init__(self, valid_processors) -> None:
        """Intialises the Command class"""

        processor: str

        ## Stuff used for the application help
        parser = argparse.ArgumentParser(description="Lists the top 10 "
                                         "applications in a repository with "
                                         " the most packages.\n\nSee "
                                         "packsort.cfg to amend repo or add "
                                         "processors.\n")
        # Parse the arguments from standard input
        help_text = self.build_help_string(valid_processors)
        parser.add_argument("proc", nargs=1, metavar='processor', type=str,
                            help=help_text)
        args = parser.parse_args()
        self.processor = args.proc[0]
        
    @staticmethod
    def build_help_string(valid_processors):
        """Creates help string for application, including valid processors
        from the configuration file."""
        # Build the help string, including the valid processors given in the
        # configuration file.
        help_text = ""
        for processor in valid_processors:
            help_text += processor + " "
        return help_text

    def __repr__(self) -> str:
        """Debugging print for Command class."""
        return '/{self.top_ten_packages}\n/{self.processor}'
