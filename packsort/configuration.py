"""This module contains the following

Classes
-------
FileManager
Configuration
"""

import logging
import sys

# FileManager borrowed from @SamyuktaSHegde
# https://www.geeksforgeeks.org/context-manager-in-python/

# TODO: Put FileManager in it's own file and use inheritance
# to create GzipManager


class FileManager():
    """Context Manager to open files"""
    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode
        self.file = None

    def __enter__(self):
        self.file = open(self.filename, self.mode)
        return self.file

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.file.close()

    def __repr__(self):
        return f"FileManager(filename={self.filename},\
                             mode={self.mode},\
                             file={self.file}"


class Configuration():
    """Class to process and access the configuration file."""

    CONFIG_FILENAME = "packsort.cfg"

    def __init__(self) -> None:
        """Intialises the Configuration class"""
        # Load the configurations (url and valid processors)
        try:
            with FileManager(self.CONFIG_FILENAME, 'rt') as file:
                self.url = file.readline().strip('\n')
                self.valid_processors = file.readline().split(' ')
                self.valid_processors[-1] = \
                    self.valid_processors[-1].strip('\n')

        except IOError as err:
            print("Error: can\'t find file or read data")
            print(err)
            sys.exit()

        # TODO: To be placed in test files
        assert self.url != '', 'URL not read in configuration initialization.'
        assert self.valid_processors != [], 'Valid processors not read in ' \
                                            'configuration initialization.'

    def check_processor(self, processor: str) -> bool:
        """Checks the processor is a valid type."""
        # TODO: To be placed in test files
        assert processor != '', 'Did not pass anything when checking if '\
                                'processor is valid.'

        if processor in self.valid_processors:
            return True
        return False

    def get_repo_url(self) -> str:
        """Gets the URL of the repo stored in the config file."""
        return self.url

    def __repr__(self) -> str:
        """Debugging print for Configuration class."""
        return f"""
        self.url = str({self.url})\n
        self.valid_processors = [{self.valid_processors}]
        """
