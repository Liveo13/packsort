"""This module contains the following

Functions
-------
entry
"""

import packsort.psapp as psapp

psapp = psapp.PSApp()


def entry():
    """Entry point for packagesorter application"""
    psapp.run()


if __name__ == "__main__":
    print("Running packagesorter...\n")
    entry()
