"""This module contains the following

Classes
-------
PSApp
"""

import sys

import packsort.configuration as configuration
import packsort.command as command
import packsort.repo as repo


class PSApp():
    """Main class for the package sorter application."""
    FILENAME_PRE = 'Contents-'
    FILENAME_EXT = '.gz'

    configuration = configuration.Configuration()
    command = command.Command(configuration.valid_processors)
    repo = repo.Repo()

    def __init__(self):
        """Intialises the PSApp class"""
        self.top_ten_packages: dict = {}

    def run(self):
        """Main run routine for the whole package."""
        print("packsort")
        print("by Paul Livesey\n")

        # First check the processor given on the command line is one
        # of those in the configuration file.
        if not self.configuration.check_processor(self.command.processor):
            print("Unknown processor.  Must be one of:\n")
            for processor in self.configuration.valid_processors:
                print(processor + " ", end="")
            print('\n\nExiting...\n')
            sys.exit()

        # Build the processor filename in the repo...
        filename = self.FILENAME_PRE + \
            self.command.processor + \
            self.FILENAME_EXT

        print("Downloading package list...\n")
        self.repo.create_package_list(self.configuration.url, filename)
        print("Sorting the package list...")
        self.top_ten_packages = self.repo.get_top_ten()

        print('\nTOP TEN PACKAGE COUNT\n---------------------')

        for count, item in enumerate(self.top_ten_packages):
            print(f'{count+1}. {item[0]}\nPackage Count = {item[1]}')
        print('\n')

        # Clear up any left over files after downloads.
        self.repo.remove_gz_files()

    def __repr__(self) -> str:
        """Debugging print for PAApp class"""
        return f'{self.top_ten_packages}'
