"""This module contains the following

Classes
-------
Repo
GzipManager
"""

from collections import Counter

import gzip
import os
import re
import sys
import requests

# GzipManager adapted from FileManager which was borrowed from 
# @SamyuktaSHegde:
# https://www.geeksforgeeks.org/context-manager-in-python/

class GzipManager():
    """Context manager to exract .gz files"""
    def __init__(self, filename, mode):
        '''Initialize file manager.  Need to check modes'''
        self.filename = filename
        self.mode = mode
        self.file = None

    def __enter__(self):
        self.file = gzip.open(self.filename, self.mode)
        return self.file

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.file.close()

    def __repr__(self):
        return f"FileManager(filename={self.filename},\
                            mode={self.mode},\
                            file={self.file}"


class Repo():
    """Class to get and process the repo package list file."""

    def __init__(self) -> None:
        """Intialises the Repo class"""
        self.package_list: dict = Counter()

    def create_package_list(self, url: str, filename: str) -> None:
        """Download the repo package list file and build it into a data structure that
        keeps a count of the number of packages per application."""

        # Do the download from the repo...
        try:
            req = requests.get(url + filename, allow_redirects=True)
            with open(filename, 'wb') as file:
                file.write(req.content)
        except requests.ConnectionError as err:
            print("Connection error when requesting file from the internet:")
            print(err)
            sys.exit()
        except requests.exceptions.HTTPError as err:
            print("HTTP error when requesting file:")
            print(err)
            sys.exit()

        # Now extract the file...
        try:
            with GzipManager(filename, 'rt') as gfile:
                # Go through the file, processing each line, one-by-one
                while True:
                    line = gfile.readline()
                    if not line:
                        break
                    components = re.findall(r"[^\s]+", line)
                    self.package_list[components[0]] = \
                        len(components[1].split(','))

        except IOError as err:
            print("Error: can\'t find file or read data")
            print(err)
            sys.exit()

    def get_top_ten(self) -> dict:
        """Sorts the package list data structure and return the top ten."""
        return self.package_list.most_common(10)

    @staticmethod
    def remove_gz_files() -> None:
        """Maintenance method - Remove any .gz files"""
        for filename in os.listdir():
            if filename.endswith(".gz"):
                os.remove(filename)

    def __repr__(self) -> str:
        """Debugging print for Repo class."""
        return "{self.package_list}"
