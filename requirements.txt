certifi==2021.10.8
charset-normalizer==2.0.11
idna==3.3
packsort==0.1.0
requests==2.27.1
urllib3==1.26.8
