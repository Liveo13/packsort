# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='packsort',
    version='0.1.0',
    description='Lists the top ten applications in a repo by number of packages.',
    long_description=readme,
    author='Paul Livesey',
    author_email='paul.livesey@protonmail.com',
    license=license,
    packages=find_packages(include=['packsort', 'packsort.*']),
    entry_points={
        'console_scripts': ['packsort=packsort.main:entry'],
    },
)

