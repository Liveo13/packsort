"""Module for tests for Repo class"""
import os
import unittest

from packsort import repo


class TestRepo(unittest.TestCase):
    """Class to test Repo class"""
    repo = repo.Repo()
    FILENAME_PRE = "Contents-"
    FILENAME_EXT = ".gz"

    def test_create_package_list(self):
        """Tests create_package_list method"""
        # TODO: Create processor list from packsort.cfg config file.
        url = "http://ftp.uk.debian.org/debian/dists/stable/main/"
        procs_to_test = ['amd64', 'i386', 's390x', 'udeb-armel', ]
        for processor in procs_to_test:
            filename = self.FILENAME_PRE + processor + self.FILENAME_EXT
            self.repo.create_package_list(url, filename)
            self.assertNotEqual(self.repo.package_list, {})
        for file in os.listdir():
            if file.endswith('.gz'):
                os.remove(file)

    def test_get_top_ten(self):
        """Tests get_top_ten() method"""
        self.assertNotEqual(self.repo.get_top_ten(), {})

    def test__repr__(self):
        """Test __repr__() method"""
        self.repo.__repr__()


if __name__ == '__main__':
    unittest.main()
